# optaplanner-aps

#### 介绍
在optaplanner官方示例的基础上开发自己的aps排程引擎，2021年3月24日12:01:31
学习optaplanner例子，理解cloudBalancing和taskassigning的例子

#### 软件架构
软件架构说明
IDEA
JDK
Maven

#### 安装教程

1.  下载项目代码和相关文件
2.  通过navicat或者其他数据库管理工具恢复项目的测试数据库，数据库为mysql
3.  运行

#### 使用说明

1.  本项目中包含了三个例子：optaplanner官方例子中的cloudBalancing和taskAssigning例子，还有一个就是我照着taskAssigning写的apsPlanning
2.  apsPlanning例子需要将初始数据从数据库载入才能运行，cloudBalancing和taskAssigning例子初始数据都是直接从自带的xml文件里面导入的
3.  xxxx，



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
