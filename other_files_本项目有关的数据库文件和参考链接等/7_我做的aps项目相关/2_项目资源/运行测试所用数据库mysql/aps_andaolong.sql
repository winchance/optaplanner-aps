/*
 Navicat Premium Data Transfer

 Source Server         : local51
 Source Server Type    : MySQL
 Source Server Version : 50172
 Source Host           : localhost:3306
 Source Schema         : aps_andaolong

 Target Server Type    : MySQL
 Target Server Version : 50172
 File Encoding         : 65001

 Date: 28/03/2021 13:11:15
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bom_task
-- ----------------------------
DROP TABLE IF EXISTS `bom_task`;
CREATE TABLE `bom_task`  (
  `bom_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'bom节点的id，主键',
  `bom_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'bom节点名称',
  `parent_bom_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父节点bom编号，没有为null',
  `child_bom_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子节点bom编号，没有为null',
  `process_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '将当前bom节点加工成父节点的工序名称',
  PRIMARY KEY (`bom_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bom_task
-- ----------------------------
INSERT INTO `bom_task` VALUES ('000102', 'bom_000102', NULL, '', 'last_pack');
INSERT INTO `bom_task` VALUES ('00010202', 'bom_00010202', '000102', '0001020203', '02');
INSERT INTO `bom_task` VALUES ('0001020203', 'bom_0001020203', '00010202', NULL, '03');
INSERT INTO `bom_task` VALUES ('000111', 'bom_000111', NULL, '', 'last_pack');
INSERT INTO `bom_task` VALUES ('00011103', 'bom_00011103', '000111', '0001110304', '03');
INSERT INTO `bom_task` VALUES ('0001110304', 'bom_0001110304', '00011103', NULL, '04');
INSERT INTO `bom_task` VALUES ('000115', 'bom_000115', NULL, '', 'last_pack');
INSERT INTO `bom_task` VALUES ('00011501', 'bom_00011501', '000115', '0001150104', '01');
INSERT INTO `bom_task` VALUES ('0001150104', 'bom_0001150104', '00011501', '', '04');
INSERT INTO `bom_task` VALUES ('000117', 'bom_000117', NULL, '', 'last_pack');
INSERT INTO `bom_task` VALUES ('00011704', 'bom_00011704', '000117', NULL, '04');
INSERT INTO `bom_task` VALUES ('000333', 'bom_000333', NULL, '00033301', 'last_pack');
INSERT INTO `bom_task` VALUES ('00033301', 'bom_00033301', '000333', '0003330101', '01');
INSERT INTO `bom_task` VALUES ('0003330101', 'bom_0003330101', '00033301', '000333010102', '01');
INSERT INTO `bom_task` VALUES ('000333010102', 'bom_000333010102', '0003330101', '', '02');
INSERT INTO `bom_task` VALUES ('000444', 'bom_000444', NULL, '00044401', 'last_pack');
INSERT INTO `bom_task` VALUES ('00044401', 'bom_00044401', '000444', '0004440103', '01');
INSERT INTO `bom_task` VALUES ('0004440103', 'bom_0004440103', '00044401', NULL, '03');
INSERT INTO `bom_task` VALUES ('000555', 'bom_000555', '', '00055502', 'last_pack');
INSERT INTO `bom_task` VALUES ('00055502', 'bom_00055502', '000555', '0005550201', '02');
INSERT INTO `bom_task` VALUES ('0005550201', 'bom_0005550201', '00055502', NULL, '01');
INSERT INTO `bom_task` VALUES ('000666', 'bom_000666', '', '00066601', 'last_pack');
INSERT INTO `bom_task` VALUES ('00066601', 'bom_00066601', '000666', '0006660101', '01');
INSERT INTO `bom_task` VALUES ('0006660101', 'bom_0006660101', '00066601', '000666010101', '01');
INSERT INTO `bom_task` VALUES ('000666010101', 'bom_000666010101', '0006660101', '00066601010101', '01');
INSERT INTO `bom_task` VALUES ('00066601010101', 'bom_00066601010101', '000666010101', NULL, '01');

-- ----------------------------
-- Table structure for device
-- ----------------------------
DROP TABLE IF EXISTS `device`;
CREATE TABLE `device`  (
  `device_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一性id',
  `device_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '设备的种类编码',
  `device_run_cost_per_hour` int(255) NOT NULL DEFAULT 0,
  PRIMARY KEY (`device_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of device
-- ----------------------------
INSERT INTO `device` VALUES ('pack_default_device', '0000', 0);
INSERT INTO `device` VALUES ('shagnhai_03_09', '0004', 200);
INSERT INTO `device` VALUES ('shanghai_03_01', '0001', 115);
INSERT INTO `device` VALUES ('shanghai_03_02', '0002', 200);
INSERT INTO `device` VALUES ('shanghai_03_03', '0003', 300);
INSERT INTO `device` VALUES ('shanghai_03_04', '0004', 60);
INSERT INTO `device` VALUES ('shanghai_03_05', '0001', 50);
INSERT INTO `device` VALUES ('shanghai_03_06', '0001', 120);
INSERT INTO `device` VALUES ('shanghai_03_07', '0002', 300);
INSERT INTO `device` VALUES ('shanghai_03_08', '0003', 200);
INSERT INTO `device` VALUES ('shanghai_03_10', '0001', 200);

-- ----------------------------
-- Table structure for device_info
-- ----------------------------
DROP TABLE IF EXISTS `device_info`;
CREATE TABLE `device_info`  (
  `device_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '共同主键',
  `machining_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '共同主键',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关于加工类型能力的描述',
  PRIMARY KEY (`machining_type`, `device_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of device_info
-- ----------------------------
INSERT INTO `device_info` VALUES ('0001', 'assemble_type', NULL);
INSERT INTO `device_info` VALUES ('0004', 'assemble_type', NULL);
INSERT INTO `device_info` VALUES ('0001', 'cut_type', NULL);
INSERT INTO `device_info` VALUES ('0003', 'cut_type', NULL);
INSERT INTO `device_info` VALUES ('0001', 'electric_welding_type', NULL);
INSERT INTO `device_info` VALUES ('0000', 'pack_type', NULL);
INSERT INTO `device_info` VALUES ('0002', 'plating_type', NULL);

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `order_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '订单id',
  `order_priority` int(20) NOT NULL COMMENT '订单优先级，数字越大越高，默认为1',
  `customer_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户id，可以用客户名字',
  `end_time` int(200) NOT NULL COMMENT '订单交期，这里先用花费分钟数',
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES ('order_amazon_202103_001', 3, 'amazon_02_06', 21600);
INSERT INTO `order` VALUES ('order_google_202107_024', 2, 'google_21_125', 80000);

-- ----------------------------
-- Table structure for process
-- ----------------------------
DROP TABLE IF EXISTS `process`;
CREATE TABLE `process`  (
  `process_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '唯一性id',
  `process_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工序名称',
  `required_machine_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '工序所用的加工功能类型',
  `process_time` int(100) NOT NULL COMMENT '加工时间，minute',
  PRIMARY KEY (`process_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of process
-- ----------------------------
INSERT INTO `process` VALUES ('01', 'cut', 'cut_type', 60);
INSERT INTO `process` VALUES ('02', 'electric welding', 'electric_welding_type', 100);
INSERT INTO `process` VALUES ('03', 'plating', 'plating_type', 30);
INSERT INTO `process` VALUES ('04', 'assemble', 'assemble_type', 10);
INSERT INTO `process` VALUES ('last_pack', 'pack', 'pack_type', 0);

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `product_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '产品id，共同主键',
  `order_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '产品所属的订单id，共同主键',
  `count` int(100) NOT NULL COMMENT '生产产品的数目',
  `bom_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '此产品对应的bom编号',
  PRIMARY KEY (`product_id`, `order_id`) USING BTREE,
  INDEX `order_id_consistency`(`order_id`) USING BTREE,
  INDEX `bom_id_consistency`(`bom_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('product_000131', 'order_amazon_202103_001', 2000, '000115');
INSERT INTO `product` VALUES ('product_000313', 'order_amazon_202103_001', 1000, '000117');
INSERT INTO `product` VALUES ('product_002111', 'order_google_202107_024', 1500, '000111');
INSERT INTO `product` VALUES ('product_003129', 'order_google_202107_024', 1200, '000102');

-- ----------------------------
-- Table structure for testtable
-- ----------------------------
DROP TABLE IF EXISTS `testtable`;
CREATE TABLE `testtable`  (
  `test_int` int(11) NULL DEFAULT NULL,
  `test_string` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of testtable
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
