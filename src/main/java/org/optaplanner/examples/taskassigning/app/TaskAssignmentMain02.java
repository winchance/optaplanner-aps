package org.optaplanner.examples.taskassigning.app;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.config.solver.EnvironmentMode;
import org.optaplanner.examples.common.app.CommonApp;
import org.optaplanner.examples.taskassigning.domain.TaskAssigningSolution;
import org.optaplanner.persistence.common.api.domain.solution.SolutionFileIO;

import java.io.File;

/**
 * @author andaolong
 * @time 2021年3月21日21:03:49
 * 这个类能初步运行了，能够通过xml加载初始数据并获得排程后的solution
 */
public class TaskAssignmentMain02 {

    public static void main(String[] args) {
        startPlan();
    }

    private static void startPlan() {

        //这里采用，例子项目的test那部分的，从xml中获取初始数据的部分代码，来获取我们的初始数据，而非手动填入
        //都是为了从xml获取初始数据，都是从test那里粘过来的
        SolutionFileIO<TaskAssigningSolution> solutionFileIO;
        CommonApp commonApp = createCommonApp();
        solutionFileIO = commonApp.createSolutionFileIO();

        final String UNSOLVED_DATA_FILE = "data/taskassigning/unsolved/50tasks-5employees.xml";

        //testData(UNSOLVED_DATA_FILE, "[0]hard/[-3925/-6293940/-7772/-20500]soft", EnvironmentMode.REPRODUCIBLE);
        //这里，貌似到了他那个传进去的bestScoreLimit也不会停。。我在配置的xml中设置了个10s的时间
        File unsolvedDataFile = new File(
                testData(UNSOLVED_DATA_FILE, "[0]hard/[-3925/-6293940/-7772/-20500]soft", EnvironmentMode.REPRODUCIBLE)
                        .unsolvedDataFile);


        //通过默认配置的xml文件创建一个solverFactory，然后创建一个solver求解器
        SolverFactory<TaskAssigningSolution> solverFactory = SolverFactory.createFromXmlResource("org/optaplanner/examples/taskassigning/solver/taskAssigningSolverConfig.xml");
        Solver<TaskAssigningSolution> solver = solverFactory.buildSolver();

        //新建一个待求解对象，将初始数据传入，这是手动
        //TaskAssigningSolution unassignment = new TaskAssigningSolution(id,skills);
        //这是，通过xml文件，加载进来
        TaskAssigningSolution unassignment = (TaskAssigningSolution) solutionFileIO.read(unsolvedDataFile);

        //调试看看加载进来了没有
        //System.out.println("main2中的unsolvedDataFile：" + unsolvedDataFile);
        //System.out.println("main2中的solutionFileIO：" + solutionFileIO);
        //System.out.println("main2中的problem：" + unassignment);


        //将带求解对象通过求解器求解
        //启动引擎，并获得排程完成后的solution
        TaskAssigningSolution assigned = solver.solve(unassignment);

        //输出一下排程结果
        System.out.println("排程分数为：" + assigned.getScore());
        System.out.println("排程结果的taskList为：" + assigned.getTaskList());
    }


    //下面这几个方法都是为了从xml获取初始数据，都是从test那里粘过来的
    protected static TestData testData(String unsolvedDataFile, String bestScoreLimit, EnvironmentMode environmentMode) {
        return new TestData(unsolvedDataFile, bestScoreLimit, environmentMode);
    }

    protected static class TestData {

        String unsolvedDataFile;
        String bestScoreLimit;
        EnvironmentMode environmentMode;

        private TestData(String unsolvedDataFile, String bestScoreLimit, EnvironmentMode environmentMode) {
            this.unsolvedDataFile = unsolvedDataFile;
            this.bestScoreLimit = bestScoreLimit;
            this.environmentMode = environmentMode;
        }
    }

    protected static TaskAssigningApp createCommonApp() {

        return new TaskAssigningApp();
    }
}



