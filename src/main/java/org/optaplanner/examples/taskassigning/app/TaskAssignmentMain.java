package org.optaplanner.examples.taskassigning.app;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.examples.taskassigning.domain.Skill;
import org.optaplanner.examples.taskassigning.domain.TaskAssigningSolution;

import java.util.ArrayList;
import java.util.List;

/**
 * @author andaolong
 * @time 2021/3/21-15:08
 * 此类未完成，此类通过手动填入数据，来进行排程
 * 后续再做
 */
public class TaskAssignmentMain {

    public static void main(String[] args) {
        startPlan();
    }

    private static void startPlan() {
        //新建多个List，
        //根据TaskAssignmentSolution的格式进行创建
        //用来存储，一个unsolved的solution类的初始数据
        //现在这里暂时采用手工填入，后续这里改成从外部数据表自动获取
        //看了看一共有以下6项，
        //写了个id和Skill的List，下面的几个List赋值的话可以参照getSkills写
        //然后发现，给的那个xml初始数据中有500行。。。。我觉得，要是手动一个个填入进去，啊，真是个体力活，不可取。。
        long id = 0;
        List<Skill> skills = getSkills();
        //List<TaskType> taskTypes = getTaskTypes();
        //List<Customer>  customers = getCustomers();
        //List<Employee> employees = getEmployees();
        //List<Task> tasks = getTasks();


        //通过默认配置的xml文件创建一个solverFactory，然后创建一个solver求解器
        SolverFactory<TaskAssigningSolution> solverFactory = SolverFactory.createFromXmlResource("org/optaplanner/examples/taskassigning/solver/taskAssigningSolverConfig.xml");
        Solver<TaskAssigningSolution> solver = solverFactory.buildSolver();

        //新建一个待求解对象，将初始数据传入
        //TaskAssigningSolution unassignment = new TaskAssigningSolution(id, skills);

        //将带求解对象通过求解器求解
        //TaskAssigningSolution assigned = solver.solve(unassignment);//启动引擎


    }




    private static List<Skill> getSkills() {
        //根据文档中写的，在这里对数据对象进行赋值，而非使用xml文件进行读取
        // 文档中给了六种工作技能
        Skill s0 = new Skill(0, "problem solving");
        Skill s1 = new Skill(1, "Team Building");
        Skill s2 = new Skill(2, "Business Storytelling");
        Skill s3 = new Skill(3, "Risk Management");
        Skill s4 = new Skill(4, "Creative Thinking");
        Skill s5 = new Skill(5, "Strategic Planning");

        List<Skill> skills = new ArrayList<Skill>();
        skills.add(s0);
        skills.add(s1);
        skills.add(s2);
        skills.add(s3);
        skills.add(s4);
        skills.add(s5);

        return skills;
    }
    //private static List<TaskType> getTaskTypes() {
    //    //根据文档中写的，在这里对数据对象进行赋值，而非使用xml文件进行读取
    //    Skill s0 = new Skill(0, "problem solving");
    //    TaskType taskType0 = new TaskType(0 ,"IS","Improve sales",46);
    //    taskType0.setRequiredSkillList();
    //    TaskType taskType1 = new TaskType(1, "ET","expand tax",63);
    //    TaskType taskType2 = new TaskType(2, "IS","Improve sales",46);
    //    TaskType taskType3 = new TaskType(3, "IS","Improve sales",46);
    //    TaskType taskType4 = new TaskType(4, "IS","Improve sales",46);
    //    TaskType taskType5 = new TaskType(5, "IS","Improve sales",46);
    //
    //
    //    List<Skill> skills = new ArrayList<Skill>();
    //    skills.add(s0);
    //    skills.add(s1);
    //    skills.add(s2);
    //    skills.add(s3);
    //    skills.add(s4);
    //    skills.add(s5);
    //
    //    return taskType0;
    //}


}



