/*
 * Copyright 2020 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.taskassigning.domain.solver;

import org.optaplanner.examples.taskassigning.domain.Task;

import java.util.Comparator;

//允许按难度对计划实体的集合进行排序。困难度权重估计计划某个PlanningEntity的难度。一些算法得益于首先/最后对较困难的计划实体进行计划，或者从专注于它们而受益。
//比较器应该以难度递增的方式排序（即使许多优化算法都会将其逆转）。例如：根据其RAM使用要求对3个进程进行困难排序：进程B（1GB RAM），进程A（2GB RAM），进程C（7GB RAM），

//在这里,是首先按照任务的优先级进行排序,然后优先级相同的情况下按照需要的skill的个数进行排序,以此类推,其实只是排了一下序,
//因为有的方法针对排完序的规划实体排程的时候速度更快,所以要排一下序
public class TaskDifficultyComparator implements Comparator<Task> {

    private static final Comparator<Task> COMPARATOR =
            Comparator.comparing(Task::getPriority)
            .thenComparingInt(task -> task.getTaskType().getRequiredSkillList().size())
            .thenComparingInt(task -> task.getTaskType().getBaseDuration())
            .thenComparingLong(Task::getId);

    @Override
    public int compare(Task a, Task b) {
        return COMPARATOR.compare(a, b);
    }
}
