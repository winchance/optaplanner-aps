package org.optaplanner.examples.apsplanning.domain;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.InverseRelationShadowVariable;
import org.optaplanner.examples.common.domain.AbstractPersistable;

/**
 * @author andaolong
 * @time 2021/3/27-10:22
 * @describe 仿照TaskOrEmployee
 */
@PlanningEntity
public abstract class BomTaskOrDevice extends AbstractPersistable {

    // Shadow variables
    // 反向关系的影子变量，这里只有这一个变量
    // 作用是，建立双向关系，因为之前的那个previousXXX向前找，这里加上这个反向的影子变量在需要从前向后找的时候就很方便了
    // 在双向关系中，影子端（=从属端）使用此属性（而不是其他任何属性）声明它对于哪个PlanningVariable（=主端）而言是影子。
    // 也就是说，在这里，TaskOrEmployee用nextTask属性来声明它对于previousTaskOrEmployee是影子
    @InverseRelationShadowVariable(sourceVariableName = "previousBomTaskOrDevice")
    protected BomTask nextBomTask;

    public BomTaskOrDevice() {
    }

    public BomTaskOrDevice(long id) {
        super(id);
    }

    public BomTask getNextBomTask() {
        return nextBomTask;
    }

    public void setNextBomTask(BomTask nextBomTask) {
        this.nextBomTask = nextBomTask;
    }

    /**
     * @return sometimes null
     */
    public abstract Integer getEndTime();

    /**
     * @return sometimes null
     */
    public abstract Device getDevice();

}
