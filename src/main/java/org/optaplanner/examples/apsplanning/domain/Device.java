package org.optaplanner.examples.apsplanning.domain;

import org.optaplanner.examples.common.swingui.components.Labeled;

import java.util.List;

/**
 * @author andaolong
 * @time 2021/3/27-10:07
 * @describe 对应taskassigning中的employee
 */

public class Device extends BomTaskOrDevice implements Labeled {

    //device，有两个属性，一个是device的唯一性编码，一个是该device具有的加工类型
    private String deviceId;
    private int deviceRunCostPerHour;

    private List<MachiningType> machiningTypes;

    public Device() {
    }


    public Device(long id, String deviceId, int deviceRunCostPerHour) {
        super(id);
        this.deviceId = deviceId;
        this.deviceRunCostPerHour = deviceRunCostPerHour;
        List<MachiningType> machiningTypes;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public int getDeviceRunCostPerHour() {
        return deviceRunCostPerHour;
    }

    public void setDeviceRunCostPerHour(int deviceRunCostPerHour) {
        this.deviceRunCostPerHour = deviceRunCostPerHour;
    }

    public List<MachiningType> getMachiningTypes() {
        return machiningTypes;
    }

    public void setMachiningType(List<MachiningType> machiningTypes) {
        this.machiningTypes = machiningTypes;
    }


    // ************************************************************************
    // Complex methods
    // ************************************************************************

    @Override
    public Device getDevice() {
        return this;
    }

    @Override
    public Integer getEndTime() {
        return 0;
    }


    @Override
    public String getLabel() {
        return deviceId;
    }


    @Override
    public String toString() {
        return deviceId;
    }

}
