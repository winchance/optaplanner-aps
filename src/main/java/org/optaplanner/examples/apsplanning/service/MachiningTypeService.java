package org.optaplanner.examples.apsplanning.service;

import org.optaplanner.examples.apsplanning.dao.MachiningTypeDao;
import org.optaplanner.examples.apsplanning.domain.MachiningType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author andaolong
 * @time 2021/3/27-14:46
 * @describe
 */
@Service
public class MachiningTypeService {

    @Autowired
    private MachiningTypeDao machiningTypeDao;

    public List<MachiningType> getMachiningTypeList(String deviceId) {
        return machiningTypeDao.getMachiningTypeListByDeviceIdFromDB(deviceId);
    }

}

