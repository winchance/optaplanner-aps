package org.optaplanner.examples.apsplanning.service;

import org.optaplanner.examples.apsplanning.dao.DeviceDao;
import org.optaplanner.examples.apsplanning.dao.MachiningTypeDao;
import org.optaplanner.examples.apsplanning.domain.Device;
import org.optaplanner.examples.apsplanning.domain.MachiningType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author andaolong
 * @time 2021年3月27日11:25:15
 */
@Service
public class DeviceService {

    @Autowired
    private DeviceDao deviceDao;
    @Autowired
    private MachiningTypeDao MachiningTypeDao;

    //获取到device，并填入machiningType的list
    public List<Device> getDeviceList() {

        List<Device> deviceList = deviceDao.getDeviceListFromDB();

        for (Device device : deviceList){
            //根据device的deviceId获取到device对应的machiningType的list
            List<MachiningType> machiningTypeList = MachiningTypeDao.getMachiningTypeListByDeviceIdFromDB(device.getDeviceId());
            //将machiningType的list填入每个device
            device.setMachiningType(machiningTypeList);
        }
            return deviceList;
    }

}

