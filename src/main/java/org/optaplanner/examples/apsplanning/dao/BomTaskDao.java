package org.optaplanner.examples.apsplanning.dao;

import org.optaplanner.examples.apsplanning.domain.BomTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author andaolong
 * @time 2021/3/27-15:06
 * @describe
 */
@Component
public class BomTaskDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    Long taskPlanningId = 10L;

    //查询所有的device的id，返回device类型的list，里面还未填入machiningType
    public List<BomTask> getBomTaskListFromDB() {

        //首先查询到所有的bom节点，还没传入Process
        String sql = "select * from bom_task";
        //将查询到的所有bomTask存到一串<BomTask>的list里面
        //return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(BomTask.class));

        //给从数据库中获得的bomTask添加上id，排程的时候需要依靠这个id进行移动操作
        //因为这个id只能通过构造函数传入，所以，这里先把所有属性取出来再加上id通过构造函数新建一个对象，然后返回新对象
        List<BomTask> bomTaskList = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(BomTask.class));
        List<BomTask> bomTaskListWithSuperId = new ArrayList<BomTask>();

        for (BomTask bomTask : bomTaskList) {

            BomTask bomTaskNew = new BomTask(taskPlanningId, bomTask.getBomId(), bomTask.getBomName(), bomTask.getParentBomId(), bomTask.getChildBomId(), bomTask.getProcessId());
            bomTaskListWithSuperId.add(bomTaskNew);
            taskPlanningId = taskPlanningId + 1L;
        }


        //先把所有的bomTask节点获取到并且附上taskPlaningId以后才可以进行子节点绑定，不然绑定上去的子节点没有taskPlanningId，排程的时候不会改变这些子节点
        //获取到该bomTask节点的所有子节点和对应的父节点，然后传入进去
        List<BomTask> bomTaskListWithSuperIdNew = new ArrayList<BomTask>();
        //List<BomTask> childBomList = new ArrayList<BomTask>();//子节点list临时变量
        BomTask parentBom = null;//父节点暂存临时变量

        for (BomTask bomTask : bomTaskListWithSuperId) {
            //一定记得要清空啊啊啊
            /*
                childBomList.clear();
                子节点list临时变量，这个不能在上面定义然后每次清空，不然最后赋值的时候赋值的全是最后一次的值
            */
            List<BomTask> childBomList = new ArrayList<BomTask>();
            parentBom = null;

            //遍历每个结点得到其子节点的list和父节点
            for (BomTask bomTask02 : bomTaskListWithSuperId) {
                //如果是子节点那么就把它填入子节点的list
                if (bomTask.getBomId().equals(bomTask02.getParentBomId())) {
                    childBomList.add(bomTask02);
                }
                //如果是父节点那么就先存到父节点的临时变量里
                if (bomTask.getParentBomId() != null && bomTask.getParentBomId().equals(bomTask02.getBomId())) {
                    parentBom = bomTask02;
                }
            }
            //如果有父节点那就绑定上
            if (parentBom != null) {
                bomTask.setParentBom(parentBom);
            }
            //如果有子节点那也绑定上，没有子节点也绑定上一个空的list[]，至少不要让它是null
            bomTask.setChildBomList(childBomList);


            bomTaskListWithSuperIdNew.add(bomTask);

        }

       /*
        System.out.println("\n\n\n=======下面是外层=======");
        int i = 1;
        for (BomTask bomTaskEx : bomTaskListWithSuperIdNew) {
            System.out.println(i + ":外层节点遍历：" + bomTaskEx);
            System.out.println("\t父节点是：" + bomTaskEx.getParentBom());
            System.out.println("\t子节点开始打印：");
            int j = 1;
            if (bomTaskEx.getChildBomList() != null && bomTaskEx.getChildBomList().size() != 0) {
                for (BomTask bomTaskIn : bomTaskEx.getChildBomList()) {
                    System.out.println("\t\t" + j + ":" + bomTaskIn);
                    j++;
                }
            }
            i++;
        }
        */
        return bomTaskListWithSuperIdNew;

    }

}
