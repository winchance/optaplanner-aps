package org.optaplanner.examples.cloudbalancing.dao;

import org.optaplanner.examples.cloudbalancing.domain.CloudProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author andaolong
 * @time 2021/3/25-9:25
 */
@Component
public class CloudProcessDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<CloudProcess> getCloudProcessListFromDB() {

        String sql = "select * from process";
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(CloudProcess.class));
    }
}
