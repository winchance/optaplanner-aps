/*
 * Copyright 2012 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.optaplanner.examples.cloudbalancing.app;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.examples.cloudbalancing.domain.CloudBalance;
import org.optaplanner.examples.cloudbalancing.domain.CloudComputer;
import org.optaplanner.examples.cloudbalancing.domain.CloudProcess;
import org.optaplanner.examples.cloudbalancing.persistence.CloudBalancingGenerator;

/**
 * To benchmark this solver config, run {CloudBalancingBenchmarkHelloWorld} instead.
 * 想要对当前的求解器配置进行基准测试，运行CloudBalancingBenchmarkHelloWorld类吧
 */
public class CloudBalancingHelloWorld {

    public static void main(String[] args) {
        // Build the Solver===通过配置的xml文件，构造求解器
        SolverFactory<CloudBalance> solverFactory = SolverFactory.createFromXmlResource(
                "org/optaplanner/examples/cloudbalancing/solver/cloudBalancingSolverConfig.xml");
        Solver<CloudBalance> solver = solverFactory.buildSolver();
        System.out.println("通过配置的xml文件，构造求解器");

        // Load a problem with 400 computers and 1200 processes===加载一个4机器12进程的云平衡例子
        //这里使用的CloudBalancingGenerator类的作用就是通过随机数生成一个，(满足要求的机器数目和进程数目)的,云平衡solution的初始数据对象
        CloudBalance unsolvedCloudBalance = new CloudBalancingGenerator().createCloudBalance(4, 12);
        System.out.println(unsolvedCloudBalance.getComputerList());
        System.out.println(unsolvedCloudBalance.getProcessList());
        System.out.println(unsolvedCloudBalance.getScore());
        System.out.println(unsolvedCloudBalance.getId());
        //System.out.println(unsolvedCloudBalance.getClass());
        System.out.println("加载一个400机器-1200进程的例子");

        // Solve the problem===启动引擎求解问题
        CloudBalance solvedCloudBalance = solver.solve(unsolvedCloudBalance);
        System.out.println("通过引擎求解问题");

        // Display the result===输出结果
        System.out.println(
                "\nSolved cloudBalance with 400 computers and 1200 processes:\n"
                        + "求解结果为:\n"
                        + toDisplayString(solvedCloudBalance));
    }

    //输出结果的方法
    public static String toDisplayString(CloudBalance cloudBalance) {
        //新建一个StringBuilder，相对于String的好处是可以持续增加字符串长度而不用重新分配空间
        StringBuilder displayString = new StringBuilder();
        //遍历云平衡的进程，获取到给每个进程分配的机器
        for (CloudProcess process : cloudBalance.getProcessList()) {
            CloudComputer computer = process.getComputer();
            //然后将分配的机器输出
            displayString.append("  ")
                    .append("进程" + process.getLabel()).append(" 分配的机器是-------> ")
                    .append(computer == null ? null : computer.getLabel()).append("\n");
        }
        return displayString.toString();
    }

}
