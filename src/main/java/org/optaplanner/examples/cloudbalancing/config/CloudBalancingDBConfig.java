package org.optaplanner.examples.cloudbalancing.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration //配置类
@ComponentScan(basePackages = "org.optaplanner.examples.cloudbalancing") //组件扫描
public class CloudBalancingDBConfig {

    //创建数据库连接池
    @Bean
    public DruidDataSource getDruidDataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/cloud_balancing_test");

        //这里按照自己本地的设置进行更改
        //dataSource.setUsername("root");
        //dataSource.setPassword("admin");

        //andaolong:2021年3月23日09:40:36
        dataSource.setUsername("root");
        dataSource.setPassword("260918mine");

        return dataSource;
    }

    //创建JdbcTemplate对象
    @Bean
    public JdbcTemplate getJdbcTemplate(DataSource dataSource) {
        //到ioc容器中根据类型找到dataSource
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        //注入dataSource
        jdbcTemplate.setDataSource(dataSource);
        return jdbcTemplate;
    }

}
