package org.optaplanner.examples.cloudbalancing.service;

import org.optaplanner.examples.cloudbalancing.dao.CloudComputerDao;
import org.optaplanner.examples.cloudbalancing.domain.CloudComputer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author andaolong
 * @time 2021/3/25-10:10
 */
@Service
public class ComputerService {

    @Autowired
    private CloudComputerDao cloudComputerDao;

    public List<CloudComputer> getComputerList() {
        return cloudComputerDao.getCloudComputerListFromDB();
    }

}

