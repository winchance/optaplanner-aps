package org.optaplanner.examples.cloudbalancing.service;

import org.optaplanner.examples.cloudbalancing.dao.CloudProcessDao;
import org.optaplanner.examples.cloudbalancing.domain.CloudProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author andaolong
 * @time 2021/3/25-10:26
 */
@Service
public class ProcessService {

    @Autowired
    private CloudProcessDao cloudProcessDao;

    public List<CloudProcess> getProcessList() {
        return cloudProcessDao.getCloudProcessListFromDB();
    }

}


